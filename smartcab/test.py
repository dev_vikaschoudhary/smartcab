# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 12:31:57 2017

@author: vikas
"""

# Import supplementary visualizations code visuals.py
import visuals as vs

# Pretty display for notebooks

#vs.plot_trials("smartcab\logs\sim_default-learning.csv")
vs.plot_trials("smartcab\logs\sim_improved-learning.csv")
